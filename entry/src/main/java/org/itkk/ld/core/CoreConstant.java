package org.itkk.ld.core;

/**
 * 常量类
 */
public class CoreConstant {
    /**
     * 设备标识(空串表示当前设备)
     */
    public static final String DEVICE_ID="";
    /**
     * 应用包名
     */
    public static final String BUNDLE_NAME = "org.itkk.ld";
    /**
     * 全局背景色
     */
    public static final String GLOBAL_BACKGROUND_COLOR = "#EDEDED";
}

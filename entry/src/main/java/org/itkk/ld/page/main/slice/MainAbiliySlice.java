package org.itkk.ld.page.main.slice;

import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.PageSlider;
import ohos.agp.utils.Color;
import org.itkk.ld.ResourceTable;
import org.itkk.ld.core.CoreConstant;
import org.itkk.ld.page.BaseAbilitySlice;

import java.util.ArrayList;

public class MainAbiliySlice extends BaseAbilitySlice {

    private PageSlider pageSlider;
    private Button btn0;
    private Button btn1;
    private Button btn2;
    private Button btn3;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        this.initView();
        this.initListener();
    }

    /**
     * 初始化界面
     */
    private void initView() {
        //设置状态栏颜色
        this.getWindow().setStatusBarColor(super.getColor(ResourceTable.Color_GLOBAL_BACKGROUND_COLOR));
        //
        this.pageSlider = (PageSlider) findComponentById(ResourceTable.Id_page_slider);
        this.btn0 = (Button) findComponentById(ResourceTable.Id_btn0);
        this.btn1 = (Button) findComponentById(ResourceTable.Id_btn1);
        this.btn2 = (Button) findComponentById(ResourceTable.Id_btn2);
        this.btn3 = (Button) findComponentById(ResourceTable.Id_btn3);
        //
        this.pageSlider.setProvider(new TestPagerProvider(getData(), super.getColor(ResourceTable.Color_GLOBAL_BACKGROUND_COLOR)));
    }

    /**
     * 初始化监听器
     */
    private void initListener() {
        this.btn0.setClickedListener(listener -> this.pageSlider.setCurrentPage(0));
        this.btn1.setClickedListener(listener -> this.pageSlider.setCurrentPage(1));
        this.btn2.setClickedListener(listener -> this.pageSlider.setCurrentPage(2));
        this.btn3.setClickedListener(listener -> this.pageSlider.setCurrentPage(3));
    }

    private ArrayList<TestPagerProvider.DataItem> getData() {
        ArrayList<TestPagerProvider.DataItem> dataItems = new ArrayList<>();
        dataItems.add(new TestPagerProvider.DataItem("Page A"));
        dataItems.add(new TestPagerProvider.DataItem("Page B"));
        dataItems.add(new TestPagerProvider.DataItem("Page C"));
        dataItems.add(new TestPagerProvider.DataItem("Page D"));
        return dataItems;
    }
}

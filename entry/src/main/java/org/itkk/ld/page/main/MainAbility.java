package org.itkk.ld.page.main;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import org.itkk.ld.page.main.slice.MainAbiliySlice;

public class MainAbility extends Ability {
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbiliySlice.class.getName());
    }
}
